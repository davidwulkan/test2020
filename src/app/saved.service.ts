import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SavedService {  

  postCollection:AngularFirestoreCollection;
    public getSaved(userId){
      this.postCollection = this.db.collection(`users/${userId}/posts`);
      return this.postCollection.snapshotChanges().pipe(map( 
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
      ))
    } 

    public deleteSaved(userId:string , id:string){
      console.log(id);
      this.db.doc(`users/${userId}/posts/${id}`).delete();
    }


  

  constructor(private db:AngularFirestore) { }
}
