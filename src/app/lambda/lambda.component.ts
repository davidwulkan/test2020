import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lambda',
  templateUrl: './lambda.component.html',
  styleUrls: ['./lambda.component.css']
})
export class LambdaComponent implements OnInit {

  x:number;
  y:number;
  result:number;

  onSubmit(){
    this.lambdaService.lambda(this.x, this.y).subscribe(
      res => {
        console.log(res);
        this.result = res;       
      }
    )
  }

  
  constructor(private lambdaService: LambdaService) { }

  ngOnInit(): void {
  }

}